ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'YYYY-MM-DD HH24:MI:SS'; -- ISO 8601
ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD HH24:MI:SS'; -- ISO 8601
SET SERVEROUTPUT ON FORMAT WRAPPED;

CREATE OR REPLACE PROCEDURE recomendaciones( varNombre IN VARCHAR2 ) IS
    CURSOR cRecomendaciones IS
        SELECT ASCII_Usuarios.nombre nombreUsuario, base.nombre nombreBase, recomendacion.nombre nombreRecomendacion
        FROM ASCII_Recomendaciones 
            JOIN ASCII_Usuarios USING (carnet) 
            JOIN ASCII_Juegos base ON (base = base.codigo) 
            JOIN ASCII_Juegos recomendacion ON (recomendacion = recomendacion.codigo)
        WHERE UPPER(base.nombre) = UPPER(varNombre);
BEGIN
    FOR recomendacion IN cRecomendaciones
    LOOP
        DBMS_OUTPUT.PUT_LINE('Si te gustó ' || recomendacion.nombreBase || ', ' || recomendacion.nombreUsuario || ' te recomienda jugar a ' || recomendacion.nombreRecomendacion);
    END LOOP;
END;
/

BEGIN
    recomendaciones('Love Letter');
END;
/

--JOIN (SELECT carnet, catID, count(*) catCnt
--    FROM ASCII_Prestamos t1 JOIN ASCII_categoriasJuegos USING (codigo)
--    GROUP BY carnet, catID
--    HAVING count(*) = (SELECT max(count(*)) FROM ASCII_Prestamos t2 JOIN ASCII_CategoriasJuegos USING (codigo) GROUP BY carnet, catID HAVING t2.carnet = t1.carnet)
--    ORDER BY catCnt DESC
--)
--USING (carnet)
--JOIN ASCII_Categorias cat USING (catID)

CREATE OR REPLACE PROCEDURE rankingJugadores ( numMax IN NUMBER ) IS
    CURSOR cJugadores IS
        SELECT carnet, apodo, cnt, totalHours, totalHours/(CURRENT_DATE - fAlta)*365 avgHours
        FROM ASCII_Usuarios
            JOIN (SELECT carnet, count(*) cnt, sum(extract(hour from (fdevolucion-fprestamo))) totalHours
                FROM ASCII_Prestamos
                GROUP BY carnet
                ORDER BY cnt DESC
                FETCH FIRST numMax ROWS ONLY
            )
            USING (carnet);
        
    nRanking    NUMBER  := 1;
    catNombre   ASCII_Categorias.nombre%TYPE;
    catCnt      NUMBER;
BEGIN
    DBMS_OUTPUT.PUT_LINE('TOP ' || numMax || ' JUGADORES');

    FOR usuario IN cJugadores
    LOOP
        DBMS_OUTPUT.PUT_LINE(nRanking || ':    ' || usuario.apodo || ' (' || usuario.cnt || ')');
        DBMS_OUTPUT.PUT_LINE('   Tiempo total de juego: ' || usuario.totalHours || ' horas (' || round(usuario.avgHours,2) || ' horas/año)');
        
        SELECT nombre, count(*) INTO catNombre, catCnt
        FROM ASCII_Prestamos prestamos JOIN ASCII_Categoriasjuegos USING (codigo) JOIN ASCII_Categorias USING (catID)
        WHERE usuario.carnet = prestamos.carnet
        GROUP BY carnet, nombre, catID
        ORDER BY count(*) DESC
        FETCH FIRST ROW ONLY;
        
        DBMS_OUTPUT.PUT_LINE('   Categoría favorita: ' || catNombre || ' (' || catCnt || ' partidas)');
        nRanking := nRanking+1;
    END LOOP;
END;
/

BEGIN
    rankingJugadores(5);
END;
/

ALTER TABLE ASCII_Materiales ADD prestado NUMBER (20) DEFAULT NULL;

CREATE OR REPLACE TRIGGER modificarPrestamo AFTER
INSERT OR DELETE OR UPDATE OF fDevolucion
ON ASCII_Prestamos
FOR EACH ROW
DECLARE
    FUTURE_DATE     EXCEPTION;
    NOT_AVAILABLE   EXCEPTION;
    
    prestado        NUMBER;
BEGIN
    IF DELETING THEN
        UPDATE ASCII_Materiales SET prestado = NULL WHERE codigo = :old.codigo;
    ELSIF INSERTING THEN
        SELECT prestado INTO prestado
        FROM ASCII_Materiales
        WHERE codigo = :new.codigo;
        
        IF prestado IS NOT NULL THEN
            RAISE NOT_AVAILABLE;
        ELSE
            UPDATE ASCII_Materiales SET prestado = :new.carnet WHERE codigo = :new.codigo;
        END IF;
    ELSE
        IF :new.fDevolucion < CURRENT_TIMESTAMP THEN
            UPDATE ASCII_Materiales SET prestado = NULL WHERE codigo = :new.codigo;
        ELSE
            RAISE FUTURE_DATE;
        END IF;
    END IF;
EXCEPTION
    WHEN FUTURE_DATE THEN
        raise_application_error(-20002, 'La fecha de devolucion esta en el futuro');
        -- No es necesario hacer rollback pues raise_application_error lo hace por nosotros
    WHEN NOT_AVAILABLE THEN
        raise_application_error(-20001, 'El juego no está disponible para el prestamo');
END;
/

INSERT INTO ASCII_Prestamos (codigo, carnet) VALUES (527398674, 71643090135340875744);
SELECT * FROM ASCII_Materiales WHERE codigo = 527398674;
UPDATE ASCII_Prestamos SET fDevolucion = CURRENT_TIMESTAMP WHERE codigo = 527398674 AND carnet = 71643090135340875744;
DELETE ASCII_Prestamos WHERE codigo = 527398674 AND carnet = 71643090135340875744;