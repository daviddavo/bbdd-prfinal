ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'YYYY-MM-DD HH24:MI:SS'; -- ISO 8601
SET DEFINE OFF;

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (507490311, 'Juegos', '2018-09-09 13:47:18', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (507490311, 'Jenga', 1, 8, 2452);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (507490311, 1032); -- Action / Dexterity
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (507490311, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (170880022, 'Juegos', '2016-07-10 18:28:06', 'Usable', 'Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (170880022, 'Mafia de cuba', 6, 12, 176558);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (170880022, 1023); -- Bluffing
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (170880022, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (170880022, 1033); -- Mafia
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (170880022, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (360700172, 'Juegos', '2017-04-07 11:57:59', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (360700172, 'London after midnight', 2, 8, 169062);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (360700172, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (360700172, 1024); -- Horror

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (84694334, 'Juegos', '2012-10-07 12:38:12', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (84694334, 'Bonanza', 2, 7, 11);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (84694334, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (84694334, 1013); -- Farming
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (84694334, 1026); -- Negotiation

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (758589576, 'Juegos', '2004-02-10 10:18:12', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (758589576, 'Time''s up!', 4, 18, 1353);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (758589576, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (758589576, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (45166485, 'Juegos', '2005-02-02 18:38:44', 'Usable', 'Amarillo Básico');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (45166485, 'Jungle Speed', 2, 8, 8098);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (45166485, 1032); -- Action / Dexterity
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (45166485, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (45166485, 1030); -- Party Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (45166485, 1037); -- Real-time

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (647931245, 'Juegos', '2005-08-09 16:18:38', 'Usable', 'Blanco Expansión');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (647931245, 'Jungle Speed', 2, 8, 8098);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (647931245, 1032); -- Action / Dexterity
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (647931245, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (647931245, 1030); -- Party Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (647931245, 1037); -- Real-time

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (626508891, 'Juegos', '2015-06-22 11:07:26', 'Usable', 'Sin enfundar');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (626508891, 'Ugha Bugha', 3, 6, 106753);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (626508891, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (626508891, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (626508891, 1045); -- Memory
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (626508891, 1030); -- Party Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (626508891, 1036); -- Prehistoric

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (585184362, 'Juegos', '2012-04-16 13:48:37', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (585184362, 'Dados Zombie', 2, 99, 62871);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (585184362, 1017); -- Dice
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (585184362, 1024); -- Horror
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (585184362, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (585184362, 1030); -- Party Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (585184362, 2481); -- Zombies

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (822999051, 'Juegos', '2017-11-05 14:33:35', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (822999051, 'Trivial portable', 2, 6, 40908);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (822999051, 1030); -- Party Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (822999051, 1027); -- Trivia

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (180014736, 'Juegos', '2018-12-25 13:06:27', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (180014736, 'Star Wars: Imperio vs Rebelión', 2, 2, 160964);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (180014736, 1023); -- Bluffing
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (180014736, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (180014736, 1064); -- Movies / TV / Radio theme
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (180014736, 1016); -- Science Fiction

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (703854585, 'Juegos', '2011-12-03 11:58:03', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (703854585, 'Mikado', 2, 6, 6424);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (703854585, 1032); -- Action / Dexterity
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (703854585, 1041); -- Children's Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (703854585, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (268609271, 'Juegos', '2009-08-02 09:18:24', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (268609271, 'Ajedrez portable', 2, 2, 171);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (268609271, 1009); -- Abstract Strategy

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (870139361, 'Juegos', '2010-09-09 12:38:27', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (870139361, 'Pictionary', 3, 16, 2281);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (870139361, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (982622725, 'Juegos', '1998-10-02 16:34:10', 'Incunable', 'Sin fundas');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (982622725, 'Guillotine', 2, 5, 116);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (982622725, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (982622725, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (982622725, 1051); -- Napoleonic

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (369113628, 'Juegos', '2017-01-02 17:22:08', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (369113628, 'La criatura', 2, 15, 217007);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (369113628, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (369113628, 1040); -- Murder/Mystery
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (369113628, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (561655840, 'Juegos', '2011-09-22 16:09:44', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (561655840, 'Dobble', 2, 8, 63268);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (561655840, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (561655840, 1041); -- Children's Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (561655840, 1094); -- Educational
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (561655840, 1030); -- Party Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (561655840, 1037); -- Real-time
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (561655840, 1038); -- Sports

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (467479744, 'Juegos', '2016-07-18 14:39:27', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (467479744, '¡Sí, Señor Oscuro!', 4, 16, 18723);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (467479744, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (467479744, 1010); -- Fantasy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (467479744, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (467479744, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (206123333, 'Juegos', '2011-05-08 09:31:12', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (206123333, 'Fluxx', 2, 6, 258);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (206123333, 1002); -- Card Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (335793936, 'Juegos', '2012-06-03 09:58:35', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (335793936, 'Rumble in the dungeon', 3, 6, 130827);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (335793936, 1023); -- Bluffing
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (335793936, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (335793936, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (571247667, 'Juegos', '2015-05-17 13:12:26', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (571247667, 'Sushi Go!', 2, 5, 133473);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (571247667, 1002); -- Card Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (331049936, 'Juegos', '2017-08-14 15:54:33', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (331049936, 'Cards Against Downtime', 3, 99, 238825);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (331049936, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (331049936, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (331049936, 1030); -- Party Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (331049936, 1120); -- Print & Play

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (643443091, 'Juegos', '2010-01-31 13:15:54', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (643443091, 'Hanabi', 2, 5, 98778);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (643443091, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (643443091, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (643443091, 1045); -- Memory

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (672049130, 'Juegos', '2018-04-16 09:05:17', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (672049130, 'Los inseparables', 2, 5, 171668);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (672049130, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (672049130, 1065); -- World War I

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (131733386, 'Juegos', '2016-11-14 09:03:24', 'Usable', 'Expansión Imploding');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (131733386, 'Exploding kittens', 2, 5, 172225);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (131733386, 1089); -- Animals
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (131733386, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (131733386, 1116); -- Comic Book / Strip
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (131733386, 1079); -- Humor

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (653241348, 'Juegos', '2008-11-29 13:55:03', 'Incunable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (653241348, 'Mini Trivial', 2, 24, 2952);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (653241348, 1030); -- Party Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (653241348, 1027); -- Trivia

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (879139789, 'Juegos', '2003-10-06 12:07:42', 'Usable', 'Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (879139789, 'El Capitán Alatriste', 2, 6, 11037);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (879139789, 1093); -- Novel-based

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (698357582, 'Juegos', '2014-09-16 19:40:37', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (698357582, 'Alta Tensión', 2, 6, 2651);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (698357582, 1021); -- Economic
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (698357582, 1088); -- Industry / Manufacturing

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (587137439, 'Juegos', '2013-04-06 14:49:41', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (587137439, 'La isla prohibida', 2, 4, 65244);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (587137439, 1022); -- Adventure
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (587137439, 1010); -- Fantasy

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (527398674, 'Juegos', '2017-05-15 17:31:24', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (527398674, 'Coup', 2, 6, 131357);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (527398674, 1023); -- Bluffing
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (527398674, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (527398674, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (527398674, 1030); -- Party Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (527398674, 1001); -- Political

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (518990411, 'Juegos', '2018-07-15 16:49:53', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (518990411, 'Ratland', 2, 6, 232473);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (518990411, 1089); -- Animals
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (518990411, 1023); -- Bluffing
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (518990411, 1079); -- Humor

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (128859739, 'Juegos', '2016-11-01 14:06:13', 'Usable', 'Hay que bajarse las instrucciones
');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (128859739, 'Ziggurat', 2, 4, 40942);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (128859739, 1027); -- Trivia

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (265953115, 'Juegos', '2015-10-17 10:23:15', 'Usable', 'Sin fundas');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (265953115, 'Saboteur', 3, 10, 9220);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (265953115, 1023); -- Bluffing
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (265953115, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (265953115, 1020); -- Exploration
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (265953115, 1010); -- Fantasy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (265953115, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (669879732, 'Juegos', '2009-10-03 14:06:23', 'Usable', 'Enfundado del revés');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (669879732, 'La resistencia', 5, 10, 41114);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (669879732, 1023); -- Bluffing
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (669879732, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (669879732, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (669879732, 1026); -- Negotiation
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (669879732, 1030); -- Party Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (669879732, 1016); -- Science Fiction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (669879732, 1081); -- Spies/Secret Agents

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (156146678, 'Juegos', '2004-01-01 11:34:23', 'Usable', 'Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (156146678, 'Zombies!!', 2, 6, 2471);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (156146678, 1020); -- Exploration
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (156146678, 1046); -- Fighting
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (156146678, 1024); -- Horror
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (156146678, 1047); -- Miniatures
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (156146678, 1064); -- Movies / TV / Radio theme
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (156146678, 2481); -- Zombies

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (700258093, 'Juegos', '2009-11-28 10:32:39', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (700258093, 'Zombies 2', 2, 6, 3651);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (700258093, 1042); -- Expansion for Base-game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (700258093, 1020); -- Exploration
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (700258093, 1046); -- Fighting
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (700258093, 1024); -- Horror
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (700258093, 1047); -- Miniatures
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (700258093, 2481); -- Zombies

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (670546869, 'Juegos', '2010-03-16 14:47:16', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (670546869, 'Zombies 3', 2, 6, 6113);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (670546869, 1042); -- Expansion for Base-game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (670546869, 1046); -- Fighting
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (670546869, 1024); -- Horror
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (670546869, 2481); -- Zombies

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (409476072, 'Juegos', '2018-12-17 13:47:27', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (409476072, 'Zombies 4', 2, 6, 10167);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (409476072, 1020); -- Exploration
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (409476072, 1046); -- Fighting
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (409476072, 1024); -- Horror
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (409476072, 1047); -- Miniatures
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (409476072, 1064); -- Movies / TV / Radio theme
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (409476072, 2481); -- Zombies

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (411723132, 'Juegos', '2008-04-06 20:45:16', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (411723132, 'Zombies 5', 2, 6, 20494);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (411723132, 1042); -- Expansion for Base-game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (411723132, 1024); -- Horror
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (411723132, 2481); -- Zombies

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (950526760, 'Juegos', '2016-12-24 19:29:52', 'Arreglable', 'Faltan cartas y mal enfundado');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (950526760, 'Los Hombres Lobo de Castronegro', 8, 18, 25821);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (950526760, 1023); -- Bluffing
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (950526760, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (950526760, 1024); -- Horror
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (950526760, 1040); -- Murder/Mystery
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (950526760, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (488201605, 'Juegos', '2003-06-21 15:17:58', 'Arreglable', 'Falta Reloj');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (488201605, 'Tabú', 8, 18, 25821);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (488201605, 1023); -- Bluffing
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (488201605, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (488201605, 1024); -- Horror
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (488201605, 1040); -- Murder/Mystery
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (488201605, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (903717553, 'Juegos', '2014-10-07 18:15:18', 'Usable', 'O 72 O 92');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (903717553, 'Kragmortha', 2, 8, 26859);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (903717553, 1010); -- Fantasy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (903717553, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (903717553, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (307357928, 'Juegos', '2009-10-16 10:48:29', 'Usable', 'Sin fundas');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (307357928, 'Black Stories', 2, 15, 18803);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (307357928, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (307357928, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (307357928, 1024); -- Horror
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (307357928, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (307357928, 1040); -- Murder/Mystery
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (307357928, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (196486656, 'Juegos', '2016-03-18 09:27:05', 'Usable', 'Sin fundas');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (196486656, 'Black Stories: Muertes ridiculas', 2, 15, 96345);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (196486656, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (196486656, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (196486656, 1024); -- Horror
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (196486656, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (196486656, 1040); -- Murder/Mystery
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (196486656, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (253994868, 'Juegos', '2017-06-20 18:45:04', 'Usable', 'Sin fundas
');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (253994868, 'Black Stories: Edición medieval', 2, 15, 127646);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (253994868, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (253994868, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (253994868, 1024); -- Horror
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (253994868, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (253994868, 1035); -- Medieval
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (253994868, 1040); -- Murder/Mystery
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (253994868, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (973769009, 'Juegos', '2014-12-05 16:31:25', 'Usable', 'Sin fundas');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (973769009, 'Black Stories: Crímenes reales', 2, 15, 57052);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (973769009, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (973769009, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (973769009, 1024); -- Horror
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (973769009, 1040); -- Murder/Mystery
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (973769009, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (864419312, 'Juegos', '2010-09-20 11:56:32', 'Usable', 'Sin fundas');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (864419312, 'Black Stories: Edición Misterio', 2, 15, 32743);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (864419312, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (864419312, 1010); -- Fantasy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (864419312, 1024); -- Horror
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (864419312, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (864419312, 1040); -- Murder/Mystery
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (864419312, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (195144846, 'Juegos', '2018-03-02 16:39:11', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (195144846, 'Bang! La bala!', 3, 8, 30933);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (195144846, 1055); -- American West
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (195144846, 1023); -- Bluffing
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (195144846, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (195144846, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (195144846, 1046); -- Fighting
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (195144846, 1079); -- Humor

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (393545356, 'Juegos', '2018-04-10 19:59:57', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (393545356, 'Marrakech', 2, 4, 29223);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (393545356, 1052); -- Arabian

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (930265900, 'Juegos', '2017-07-06 14:26:25', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (930265900, 'Catán', 3, 4, 13);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (930265900, 1026); -- Negotiation

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (804481452, 'Juegos', '2016-06-06 16:59:58', 'Usable', 'Expansión, caja desorganizada');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (804481452, 'King of Tokyo', 2, 6, 70323);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (804481452, 1017); -- Dice
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (804481452, 1046); -- Fighting
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (804481452, 1064); -- Movies / TV / Radio theme
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (804481452, 1016); -- Science Fiction

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (270103881, 'Juegos', '2004-09-03 16:40:19', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (270103881, '¡Aventureros al tren!', 2, 5, 9209);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (270103881, 1034); -- Trains

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (338686884, 'Juegos', '2018-06-05 13:17:30', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (338686884, 'Mysterium', 2, 7, 181304);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (338686884, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (338686884, 1040); -- Murder/Mystery
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (338686884, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (435077835, 'Juegos', '2007-01-01 14:47:43', 'Usable', 'Sin fundas
');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (435077835, 'Talismán', 2, 6, 714);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (435077835, 1022); -- Adventure
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (435077835, 1020); -- Exploration
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (435077835, 1010); -- Fantasy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (435077835, 1046); -- Fighting

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (201222074, 'Juegos', '2012-04-05 20:45:42', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (201222074, 'Fórmula D', 2, 10, 37904);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (201222074, 1031); -- Racing
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (201222074, 1038); -- Sports

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (868808044, 'Juegos', '2008-10-14 11:15:36', 'Usable', 'Sólo Francés
');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (868808044, 'Mémoire 44', 2, 8, 10630);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (868808044, 1047); -- Miniatures
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (868808044, 1019); -- Wargame
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (868808044, 1049); -- World War II

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (857051941, 'Juegos', '2000-03-20 19:21:03', 'Arreglable', 'Faltan instrucciones (Imprimir bien), faltan fichas');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (857051941, 'Yahtzee', 2, 10, 2243);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (857051941, 1041); -- Children's Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (857051941, 1116); -- Comic Book / Strip
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (857051941, 1017); -- Dice
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (857051941, 1064); -- Movies / TV / Radio theme
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (857051941, 1054); -- Music
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (857051941, 1038); -- Sports
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (857051941, 1097); -- Travel

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (147729606, 'Juegos', '2013-06-23 11:36:54', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (147729606, 'Small World Underground', 2, 5, 97786);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (147729606, 1010); -- Fantasy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (147729606, 1046); -- Fighting
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (147729606, 1086); -- Territory Building

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (290125844, 'Juegos', '2018-02-10 13:48:13', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (290125844, 'Feudal', 2, 6, 847);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (290125844, 1009); -- Abstract Strategy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (290125844, 1035); -- Medieval
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (290125844, 1047); -- Miniatures

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (251337722, 'Juegos', '2016-01-07 20:31:14', 'Usable', 'Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (251337722, 'A través del desierto', 2, 5, 503);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (251337722, 1009); -- Abstract Strategy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (251337722, 1089); -- Animals
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (251337722, 1052); -- Arabian

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (953850576, 'Juegos', '2011-08-01 16:36:19', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (953850576, 'Paparazzi', 2, 8, 24388);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (953850576, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (952346420, 'Juegos', '2014-04-01 14:40:46', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (952346420, 'Love letter', 2, 4, 129622);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (952346420, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (952346420, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (952346420, 1070); -- Renaissance

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (155192418, 'Juegos', '2008-02-12 15:07:17', 'Usable', 'Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (155192418, '¡Si, oscuro padrino!', 5, 11, 34173);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (155192418, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (155192418, 1021); -- Economic
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (155192418, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (155192418, 1033); -- Mafia
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (155192418, 1040); -- Murder/Mystery
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (155192418, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (617920706, 'Juegos', '2018-05-14 20:19:39', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (617920706, 'Locos crononautas', 2, 5, 203997);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (617920706, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (617920706, 1094); -- Educational
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (617920706, 1010); -- Fantasy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (617920706, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (617920706, 1016); -- Science Fiction

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (271974491, 'Juegos', '2004-09-22 15:02:04', 'Usable', 'Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (271974491, 'Killer Bunnies', 2, 8, 3699);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (271974491, 1089); -- Animals
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (271974491, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (271974491, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (271974491, 1026); -- Negotiation

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (727226330, 'Juegos', '2014-01-29 13:58:43', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (727226330, 'Rhino Hero', 2, 5, 91514);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (727226330, 1032); -- Action / Dexterity
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (727226330, 1089); -- Animals
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (727226330, 1041); -- Children's Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (727226330, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (850083148, 'Juegos', '2017-11-04 10:46:57', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (850083148, 'Parchís modular', 2, 6, 2136);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (850083148, 1041); -- Children's Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (850083148, 1116); -- Comic Book / Strip
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (850083148, 1017); -- Dice
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (850083148, 1064); -- Movies / TV / Radio theme
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (850083148, 1093); -- Novel-based
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (850083148, 1031); -- Racing

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (524284071, 'Juegos', '2009-07-05 12:22:14', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (524284071, 'Monopoly', 2, 8, 1406);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (524284071, 1021); -- Economic
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (524284071, 1026); -- Negotiation

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (937697389, 'Juegos', '2014-11-10 13:16:27', 'Usable', 'Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (937697389, 'Cash''n guns', 4, 6, 19237);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (937697389, 1023); -- Bluffing
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (937697389, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (937697389, 1046); -- Fighting
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (937697389, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (937697389, 1033); -- Mafia
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (937697389, 1026); -- Negotiation
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (937697389, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (954808125, 'Juegos', '2018-10-19 20:54:28', 'Usable', 'NO enfundar');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (954808125, 'Rhino Hero: Super Battle', 2, 4, 218333);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (954808125, 1032); -- Action / Dexterity
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (954808125, 1089); -- Animals
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (954808125, 1041); -- Children's Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (585707220, 'Juegos', '2011-09-07 18:05:42', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (585707220, 'Dixit', 3, 6, 39856);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (585707220, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (585707220, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (585707220, 1030); -- Party Game

INSERT INTO ASCII_Expansiones (Codigo,Titulo)
    VALUES (585707220, 'Dixit: Memories');
INSERT INTO ASCII_Expansiones (Codigo,Titulo)
    VALUES (585707220, 'Dixit: Revelations');

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (542097873, 'Juegos', '2016-05-29 19:25:16', 'Usable', 'Son dos. Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (542097873, 'Trivial pursuit', 2, 24, 2952);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (542097873, 1030); -- Party Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (542097873, 1027); -- Trivia

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (230128811, 'Juegos', '2014-05-25 09:28:43', 'Usable', 'Son dos
. Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (230128811, 'Trivial pursuit', 2, 24, 2952);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (230128811, 1030); -- Party Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (230128811, 1027); -- Trivia

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (820749428, 'Juegos', '2010-10-19 13:41:48', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (820749428, 'Trivial pursuit: Edición master', 2, 6, 87904);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (820749428, 1030); -- Party Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (820749428, 1027); -- Trivia

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (971648177, 'Juegos', '2007-02-22 15:30:18', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (971648177, 'Arkham horror', 1, 8, 15987);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (971648177, 1022); -- Adventure
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (971648177, 1010); -- Fantasy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (971648177, 1046); -- Fighting
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (971648177, 1024); -- Horror
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (971648177, 1093); -- Novel-based

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (604208493, 'Juegos', '2002-11-15 11:14:35', 'Arreglable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (604208493, 'Munchkin', 3, 6, 1927);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (604208493, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (604208493, 1010); -- Fantasy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (604208493, 1046); -- Fighting
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (604208493, 1079); -- Humor

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (685941400, 'Juegos', '2014-01-06 14:35:45', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (685941400, '7 Wonders', 2, 7, 68448);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (685941400, 1050); -- Ancient
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (685941400, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (685941400, 1029); -- City Building
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (685941400, 1015); -- Civilization

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (682329074, 'Juegos', '2006-08-14 20:45:47', 'Usable', 'Sin fundas. Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (682329074, 'Courtisans of Versailles', 3, 8, 291);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (682329074, 1026); -- Negotiation
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (682329074, 1001); -- Political

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (838188769, 'Juegos', '2003-12-15 13:01:18', 'Usable', 'Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (838188769, 'El misterio de la abadía', 2, 6, 945);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (838188769, 1017); -- Dice
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (838188769, 1045); -- Memory
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (838188769, 1054); -- Music
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (838188769, 1038); -- Sports
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (838188769, 1027); -- Trivia
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (838188769, 1025); -- Word Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (837424856, 'Juegos', '2013-05-07 09:02:55', 'Usable', 'Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (837424856, 'Martinique', 2, 2, 42244);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (837424856, 1023); -- Bluffing
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (837424856, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (837424856, 1090); -- Pirates

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (428001381, 'Juegos', '1994-10-19 18:59:22', 'Usable', 'Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (428001381, 'Scattergories', 2, 6, 2381);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (428001381, 1030); -- Party Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (428001381, 1037); -- Real-time
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (428001381, 1025); -- Word Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (85574695, 'Juegos', '2013-09-27 19:44:26', 'Usable', 'En inglés');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (85574695, 'Cards Against Humanity', 4, 30, 50381);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (85574695, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (85574695, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (85574695, 1118); -- Mature / Adult
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (85574695, 1030); -- Party Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (85574695, 1120); -- Print & Play

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (314225902, 'Juegos', '2011-01-26 13:51:54', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (314225902, 'Takenoko', 2, 4, 70919);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (314225902, 1089); -- Animals
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (314225902, 1084); -- Environmental
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (314225902, 1013); -- Farming
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (314225902, 1086); -- Territory Building

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (566157944, 'Juegos', '2002-01-11 13:35:03', 'Arreglable', 'Comprobar estado. Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (566157944, 'Dungeons & Dragons', 2, 5, 140509);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (566157944, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (566157944, 1041); -- Children's Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (566157944, 1064); -- Movies / TV / Radio theme

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (621287267, 'Juegos', '2003-06-04 12:36:05', 'Usable', 'Faltan fundas');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (621287267, 'Criaturas y cultistas', 2, 5, 2242);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (621287267, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (621287267, 1024); -- Horror
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (621287267, 1079); -- Humor

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (991317338, 'Juegos', '2018-06-12 13:19:10', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (991317338, 'Crazy therapy', 6, 16, 184190);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (991317338, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (991317338, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (991317338, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (991317338, 1030); -- Party Game

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (840655483, 'Juegos', '2015-01-13 09:31:44', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (840655483, 'Virus!', 2, 6, 180020);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (840655483, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (840655483, 2145); -- Medical

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (845078739, 'Juegos', '2004-09-09 12:20:52', 'Arreglable', 'Con expansión Dodge City, Revisar fundas
');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (845078739, 'Bang!', 4, 7, 3955);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (845078739, 1055); -- American West
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (845078739, 1023); -- Bluffing
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (845078739, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (845078739, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (845078739, 1046); -- Fighting

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (510980242, 'Juegos', '2015-07-16 10:31:38', 'Usable', 'Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (510980242, 'Ciudadelas', 2, 8, 478);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (510980242, 1023); -- Bluffing
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (510980242, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (510980242, 1029); -- City Building
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (510980242, 1010); -- Fantasy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (510980242, 1035); -- Medieval

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (243541526, 'Juegos', '2004-04-16 11:23:08', 'Arreglable', 'Faltan piezas e instrucciones en castellano');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (243541526, 'Operación', 1, 6, 3737);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (243541526, 1032); -- Action / Dexterity
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (243541526, 1041); -- Children's Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (243541526, 2145); -- Medical

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (242380829, 'Juegos', '2007-12-19 17:18:12', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (242380829, 'Agrícola', 1, 5, 31260);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (242380829, 1089); -- Animals
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (242380829, 1021); -- Economic
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (242380829, 1013); -- Farming

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (250884700, 'Juegos', '2016-02-03 20:05:34', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (250884700, 'Shinobi', 2, 4, 152847);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (250884700, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (250884700, 1010); -- Fantasy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (250884700, 1046); -- Fighting

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (862290062, 'Juegos', '2007-02-26 13:30:36', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (862290062, 'Carcassonne', 2, 5, 822);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (862290062, 1029); -- City Building
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (862290062, 1035); -- Medieval
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (862290062, 1086); -- Territory Building

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (419138522, 'Juegos', '2016-08-17 17:24:52', 'Usable', 'Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (419138522, 'Carcassonne: Cazadores y recolectores', 2, 5, 4390);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (419138522, 1036); -- Prehistoric

INSERT INTO ASCII_Expansiones (Codigo, Titulo)
    VALUES (862290062, 'Carcassonne: La princesa y el Dragón');

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (18635348, 'Juegos', '2018-03-13 12:32:33', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (18635348, 'Carcassonne: Star Wars', 2, 5, 180564);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (18635348, 1064); -- Movies / TV / Radio theme
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (18635348, 1016); -- Science Fiction

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (438834923, 'Juegos', '1995-09-22 13:45:33', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (438834923, 'Fuga de Colditz', 2, 6, 715);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (438834923, 1022); -- Adventure
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (438834923, 1049); -- World War II

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (964977585, 'Juegos', '2017-08-23 09:23:52', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (964977585, 'Magic maze', 1, 8, 209778);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (964977585, 1020); -- Exploration
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (964977585, 1010); -- Fantasy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (964977585, 1059); -- Maze
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (964977585, 1037); -- Real-time

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (430588711, 'Juegos', '2018-10-23 18:11:24', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (430588711, 'Smash Up', 2, 4, 122522);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (430588711, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (430588711, 1010); -- Fantasy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (430588711, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (430588711, 1090); -- Pirates
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (430588711, 1016); -- Science Fiction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (430588711, 2481); -- Zombies

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (766222245, 'Juegos', '2011-12-30 09:48:27', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (766222245, 'Rune Age', 1, 4, 94362);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (766222245, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (766222245, 1010); -- Fantasy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (766222245, 1046); -- Fighting

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (927955996, 'Juegos', '2006-06-27 13:52:39', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (927955996, 'Cathedral', 2, 2, 7);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (927955996, 1009); -- Abstract Strategy

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (106749821, 'Juegos', '2009-03-03 13:24:53', 'Usable', 'Rehacer caja por dentro. Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (106749821, 'Dominion', 2, 4, 36218);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (106749821, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (106749821, 1035); -- Medieval

INSERT INTO ASCII_Expansiones (Codigo, Titulo)
    VALUES (106749821, 'Dominion: Edad Oscura');

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (851235196, 'Juegos', '2010-03-29 11:21:39', 'Usable', 'Rehacer caja interna');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (851235196, 'Dominion: Intriga', 2, 4, 40834);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (851235196, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (851235196, 1035); -- Medieval

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (286701931, 'Juegos', '2001-10-28 17:37:27', 'Incunable', 'Museo. Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (286701931, 'Risk', 2, 6, 181);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (286701931, 1086); -- Territory Building
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (286701931, 1019); -- Wargame

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (657948358, 'Juegos', '2003-12-09 13:35:30', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (657948358, 'Quoridor', 2, 4, 624);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (657948358, 1009); -- Abstract Strategy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (657948358, 1059); -- Maze

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (341971670, 'Juegos', '2016-12-31 12:47:57', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (341971670, 'Khet: The Laser Game', 2, 2, 16991);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (341971670, 1009); -- Abstract Strategy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (341971670, 1072); -- Electronic

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (948690342, 'Juegos', '2011-07-26 13:03:58', 'Usable', 'Propiedaz de Loz');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (948690342, 'Twilight Struggle', 2, 2, 12333);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (948690342, 1069); -- Modern Warfare
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (948690342, 1001); -- Political
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (948690342, 1019); -- Wargame

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (489855290, 'Juegos', '2014-08-03 18:47:00', 'Lozano', 'Cartas viejunas. Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (489855290, 'Ticket to ride', 2, 5, 9209);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (489855290, 1034); -- Trains

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (980789108, 'Juegos', '2011-12-09 17:36:03', 'Usable', 'En inglés, sin fundas');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (980789108, 'El señor de los anillos', 2, 5, 823);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (980789108, 1022); -- Adventure
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (980789108, 1010); -- Fantasy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (980789108, 1093); -- Novel-based

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (481287493, 'Juegos', '2014-12-24 09:22:27', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (481287493, 'Alquimistas', 2, 4, 161970);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (481287493, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (481287493, 1010); -- Fantasy

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (890814220, 'Juegos', '2015-01-19 10:34:27', 'Lozano', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (890814220, 'Descent', 2, 2, 150586);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (890814220, 1009); -- Abstract Strategy

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (925327567, 'Juegos', '2010-07-07 10:17:48', 'Lozano', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (925327567, 'Twilight imperium', 2, 6, 24);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (925327567, 1015); -- Civilization
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (925327567, 1026); -- Negotiation
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (925327567, 1001); -- Political
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (925327567, 1016); -- Science Fiction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (925327567, 1113); -- Space Exploration
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (925327567, 1019); -- Wargame

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (887208567, 'Juegos', '2017-05-04 18:46:25', 'Usable', 'Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (887208567, 'Shogun', 3, 5, 20551);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (887208567, 1021); -- Economic
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (887208567, 1035); -- Medieval
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (887208567, 1086); -- Territory Building

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (958333580, 'Juegos', '2008-04-05 10:11:15', 'Lozano', 'Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (958333580, 'El palé', 2, 6, 37168);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (958333580, 1021); -- Economic

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (677770956, 'Juegos', '1994-08-27 19:18:56', 'Incunable', 'Sin cartas. Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (677770956, 'Risk ', 2, 6, 181);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (677770956, 1086); -- Territory Building
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (677770956, 1019); -- Wargame

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (37958394, 'Juegos', '2000-01-06 09:13:22', 'Arreglable', 'Se completa con el D&D. Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (37958394, 'HeroQuest', 2, 5, 699);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (37958394, 1022); -- Adventure
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (37958394, 1020); -- Exploration
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (37958394, 1010); -- Fantasy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (37958394, 1046); -- Fighting
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (37958394, 1047); -- Miniatures

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (9966371, 'Juegos', '2012-09-27 16:06:35', 'Lozano', 'Marina Jugó
. Con instrucciones');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (9966371, 'Spacego', 3, 5, 95230);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (9966371, 1016); -- Science Fiction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (9966371, 1019); -- Wargame

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (839730706, 'Juegos', '2016-08-28 13:42:41', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (839730706, 'King of New York', 2, 6, 160499);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (839730706, 1017); -- Dice
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (839730706, 1046); -- Fighting
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (839730706, 1064); -- Movies / TV / Radio theme
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (839730706, 1016); -- Science Fiction

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (308179286, 'Juegos', '2014-10-22 15:39:14', 'Lozano', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (308179286, 'Battletech', 2, 20, 1540);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (308179286, 1046); -- Fighting
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (308179286, 1047); -- Miniatures
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (308179286, 1016); -- Science Fiction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (308179286, 1019); -- Wargame

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (689302586, 'Juegos', '2011-10-04 20:06:24', 'Usable', 'Expansión Catán 5-6 jugadores');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (689302586, 'Catán', 3, 4, 13);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (689302586, 1026); -- Negotiation

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (127915210, 'Juegos', '2017-12-15 15:54:14', 'Lozano', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (127915210, 'BattleTech: Aerotech', 2, 8, 8196);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (127915210, 2650); -- Aviation / Flight
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (127915210, 1047); -- Miniatures
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (127915210, 1016); -- Science Fiction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (127915210, 1019); -- Wargame

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (480674548, 'Juegos', '2014-03-25 13:44:38', 'Lozano', 'Moho inside');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (480674548, 'Los Descubridores de Catán', 3, 4, 13);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (480674548, 1026); -- Negotiation

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (462839979, 'Juegos', '2015-03-22 10:14:20', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (462839979, 'Gloom', 2, 5, 12692);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (462839979, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (462839979, 1024); -- Horror
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (462839979, 1079); -- Humor

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (800955108, 'Juegos', '2014-07-15 17:32:26', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (800955108, 'Timeline: Discoveries', 2, 8, 99975);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (800955108, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (800955108, 1094); -- Educational
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (800955108, 1027); -- Trivia

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (47554194, 'Juegos', '2014-12-18 17:56:05', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (47554194, 'Timeline: Music & Cinema', 2, 8, 145189);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (47554194, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (47554194, 1094); -- Educational
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (47554194, 1027); -- Trivia

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (902512489, 'Juegos', '2018-02-20 15:55:17', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (902512489, 'Timeline: Diversity', 2, 8, 131325);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (902512489, 1002); -- Card Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (902512489, 1094); -- Educational
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (902512489, 1027); -- Trivia

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (361185658, 'Juegos', '2017-09-03 16:32:54', 'Lozano', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (361185658, 'Zombicide', 1, 6, 113924);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (361185658, 1024); -- Horror
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (361185658, 1047); -- Miniatures
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (361185658, 2481); -- Zombies

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (599287974, 'Juegos', '2015-08-15 16:01:58', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (599287974, 'El espía (que se perdió)', 3, 8, 166384);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (599287974, 1023); -- Bluffing
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (599287974, 1039); -- Deduction
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (599287974, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (599287974, 1030); -- Party Game
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (599287974, 1081); -- Spies/Secret Agents

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (659625461, 'Juegos', '2003-10-03 12:46:13', 'Usable', '');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (659625461, 'Blood bowl', 2, 2, 3071);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (659625461, 1010); -- Fantasy
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (659625461, 1038); -- Sports
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (659625461, 1019); -- Wargame

INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)
    VALUES (41169864, 'Juegos', '2013-07-30 14:19:36', 'Usable', 'Papel');
INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)
    VALUES (41169864, 'Time''s Up!', 4, 18, 1353);
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (41169864, 1079); -- Humor
INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (41169864, 1030); -- Party Game

