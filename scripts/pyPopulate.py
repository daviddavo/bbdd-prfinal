#!/usr/bin/python3
import sys
import csv
import random
import re
from datetime import date,datetime,timedelta
from time import sleep

import json
from faker import Faker
from libbgg.apiv2 import BGG
from urllib.error import HTTPError

fake = Faker("es_ES")

def randDate(start, end = datetime.now()):
    return datetime.fromtimestamp(int(random.randint(start.timestamp(), int(end.timestamp()))))

if len(sys.argv) == 2 and int(sys.argv[1]) == 1:
    usedCat = {}
    startFrom = ""  # Only for debugging
    first = True
    try:
        with open("originaldb.csv") as f, open("03PopulateGames.sql", "w+") as popFile:

            cnt = 1
            conn = BGG()
            reader = csv.reader(f, delimiter=',')
            total = sum(1 for row in reader)-1
            f.seek(0)
            next(reader)

            popFile.write("ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'YYYY-MM-DD HH24:MI:SS'; -- ISO 8601\n")
            popFile.write("SET DEFINE OFF;\n\n")
            
            for line in reader:
                if startFrom != "" and first and line[1] != startFrom: continue
                else: first = False

                try:
                    r = conn.search(line[1],exact=True)
                except HTTPError as e:
                    print(e)
                    sleep(15)
                    r = conn.search(line[1], exact=True)
                except ConnectionResetError as e:
                    print(e)
                    conn = BGG()
                    r = conn.search(line[1], exact=True)

                bggid = 0
                while (bggid == 0):
                    # print(json.dumps(r, indent=4))

                    if int(r["items"]["total"]) != 1:
                        bggid = int(input("No se ha hallado una coincidencia exacta para " + line[1] + ". Introduzca el ID (0 para reintentar): "))
                    else:
                        bggid = int(r["items"]["item"]["id"])

                codigo = random.randint(1,10**9)
                estado = line[3].replace("Bebé", "Lozano").replace("Jugable", "Usable")
                descripcion = line[5]
                if (line[4] == "si"):
                    if (descripcion != ""): descripcion += ". "
                    descripcion += "Con instrucciones"

                try:
                    r = conn._things(bggid)
                except HTTPError as e:
                    print(e)
                    sleep(15)
                    r = conn._things(bggid)
                except ConnectionResetError as e:
                    print(e)
                    conn = BGG()
                    r = conn._things(bggid)

                fecha = randDate(max(datetime.fromisoformat("1993-12-15"), datetime.fromisoformat("%04d-01-01" % (int(r["items"]["item"]["yearpublished"]["value"]))))).replace(hour=random.randint(9,20))
                popFile.write("INSERT INTO ASCII_Materiales (Codigo, Tipo, Adquisicion, Estado, Descripcion)\n    VALUES (%d, '%s', '%s', '%s', '%s');\n" %
                    (codigo, "Juegos", fecha, estado, descripcion))

                popFile.write("INSERT INTO ASCII_Juegos (Codigo,Nombre,MinJugadores,MaxJugadores,BGGId)\n    VALUES (%d, '%s', %d, %d, %d);\n" %
                    (codigo, line[1].replace("'", "''"), int(r["items"]["item"]["minplayers"]["value"]), int(r["items"]["item"]["maxplayers"]["value"]), bggid)
                )

                for category in r["items"]["item"]["link"]:
                    if category["type"] == "boardgamecategory": 
                        popFile.write("INSERT INTO ASCII_CategoriasJuegos (Codigo, CatId) VALUES (%d, %d); -- %s\n" % (codigo, int(category["id"]), category["value"]))
                        usedCat[int(category["id"])] = category["value"]

                popFile.write("\n")

                print("[%03d/%03d] Añadido %d: %s" % (cnt, total, bggid, line[1]))
                cnt += 1

    except KeyboardInterrupt:
        print()

    with open("02PopulateCategories.sql", "w+") as popCat:
        popCat.write("SET DEFINE OFF;\n\n")
        for cid in sorted(usedCat.keys()):
            popCat.write("INSERT INTO ASCII_Categorias (CatId, Nombre)\n    VALUES (%d, '%s');\n" % (cid, usedCat[cid].replace("'", "''")))
            print("Añadida categoría %d: %s" % (cid, usedCat[cid]))

elif len(sys.argv) == 3 and int(sys.argv[1]) == 2:
    # First we create the games list
    gameList = []
    print("Reading 03PopulateGames.sql")
    s = False
    with open("03PopulateGames.sql") as games:
        for line in games:
            if s: gameList.append(int(re.search(r"\(([0-9]*),.*\);", line).group(1)))
            s = "ASCII_Juegos" in line

    # Populate users given number of users to create
    print("Populating users file")
    with open("04PopulateUsers.sql", "w+") as popUsers:
        popUsers.write("ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'YYYY-MM-DD HH24:MI:SS'; -- ISO 8601\n")
        popUsers.write("ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD HH24:MI:SS'; -- ISO 8601\n")
        popUsers.write("DELETE FROM ASCII_Recomendaciones;\nDELETE FROM ASCII_Prestamos;\nDELETE FROM ASCII_Usuarios;\n")
        popUsers.write("SET DEFINE OFF;\n\n")

        i = 0
        while (i < int(sys.argv[2])):
            fAlta = randDate(datetime.fromisoformat("1993-12-15")).replace(hour=random.randint(9,20))
            carnet = random.randint(0,10**20)

            profile = fake.simple_profile()
            popUsers.write("---------------- NEW USER %s\n" % (profile["username"]))
            popUsers.write("INSERT INTO ASCII_Usuarios (Carnet, FAlta, Nombre, Apellidos, Apodo)\n    VALUES (%d, '%s', '%s', '%s', '%s');\n\n" % (carnet, fAlta, profile["name"].split(" ")[0], profile["name"].split(" ")[-1], profile["username"]))

            nPrestamos = random.randint(1,1+int((datetime.now()-fAlta).days*random.uniform(0.0,0.05)))
            j = 0
            while (j < nPrestamos):
                fPrestamo = randDate(fAlta).replace(hour=random.randint(9,19))
                if random.uniform(0,1) < 0.005:
                    # Devuelto el día siguiente
                    fDevolucion = randDate(fPrestamo, fPrestamo.replace(hour=20)+timedelta(days=1))
                else:
                    # Devuelto el mismo día
                    fDevolucion = randDate(fPrestamo, fPrestamo.replace(hour=20))

                juego = random.choice(gameList)
                popUsers.write("INSERT INTO ASCII_Prestamos (FPrestamo, Carnet, Codigo, FDevolucion)\n    VALUES ('%s', %d, %d, '%s');\n" % (fPrestamo, carnet, juego, fDevolucion))

                if random.uniform(0,1) < 0.1:
                    popUsers.write("INSERT INTO ASCII_Recomendaciones (Carnet, Base, Recomendacion)\n VALUES (%d, %d, %d);\n" % (carnet, random.choice(gameList), juego))
                j+= 1

            popUsers.write("\n\n")
            i += 1

        popUsers.write("DELETE FROM ASCII_Prestamos WHERE Codigo = %d;\n" % (random.choice(gameList)))

else:
    print("Unknown command: " + str(sys.argv))